#ifndef TICTACTOE_HPP
#define TICTACTOE_HPP

#include <array>
#include <iostream>

enum Joueur { JOUEUR_VIDE, JOUEUR_ROUGE, JOUEUR_VERT, JOUEUR_EGALITE };

class Jeu {
    private:
        Joueur grille[3][3];
        Joueur courant;

    public:
        /** \brief Constructeur à utiliser.*/
        Jeu();

        /** \brief Retourne le vainqueur (ROUGE, VERT, EGALITE) ou VIDE si partie en cours.*/
        Joueur getVainqueur() const;

        /** \brief Retourne le joueur (ROUGE ou VERT) qui doit jouer.*/
        Joueur getJoueurCourant() const;

        /** \brief Joue un coup pour le joueur courant.

           i ligne choisie (0, 1 ou 2)
         j colonne choisie (0, 1 ou 2)
         
         Si le coup est invalide, retourne false. Si le coup est valide,
         joue le coup et retourne true.*/

        bool jouer(int i, int j);
        /** \brief Réinitialise le jeu.*/
        void raz();
        
        /** \brief Change une case de couleur*/
        void modif_grille(int i, int j);

        /** \brief change de joueur apres un coup*/
        void modif_courant();

        /** verification si partie terminer*/
        Joueur analyse_grille() const;

        /** \brief verifie l'egaliter*/
        bool check_egalite() const;

        friend std::ostream & operator<<(std::ostream & os, const Jeu & jeu);
};

std::ostream & operator<<(std::ostream & os, const Jeu & jeu);

#endif

