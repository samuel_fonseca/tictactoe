#include "Tictactoe.hpp"

using namespace std;

std::string joueurToNom(Joueur joueur){
    switch (joueur)
    {
        case 0:
            return "case vide \n";
            break;
        case 1:
            return "rouge \n";
            break;
        case 2:
            return "vert \n";
            break;
        case 3:
            return "egalite \n";
            break;
        default:
            break;
    }
}


int main() {
    Jeu jeu;
    Joueur vainqueur = jeu.getVainqueur();
    jeu.raz();
    int compteur = 0, i, j;

    while(vainqueur == 0){
    
        //affichage de jeu
        cout << jeu << endl;

        cout << "Entrez un coup (i, j) pour " << joueurToNom(jeu.getJoueurCourant()) << endl;
        cin >> i >> j;

        jeu.jouer(i, j);

        vainqueur = jeu.getVainqueur();
    }
    cout << joueurToNom(vainqueur);

    return 0;
}

