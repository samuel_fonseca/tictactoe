#include "Tictactoe.hpp"

Jeu::Jeu() {
    raz();
}

void Jeu::raz() {
    
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++)
            {
                grille[i][j] = JOUEUR_VIDE;
            }
            
        }
        courant = JOUEUR_ROUGE;
}

std::ostream & operator<<(std::ostream & os, const Jeu & jeu) {
    for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++)
            { 
                if(jeu.grille[i][j] == JOUEUR_VIDE){ os<< ".";}
                else if (jeu.grille[i][j] == JOUEUR_VERT){ os<< "V";}
                else if (jeu.grille[i][j] == JOUEUR_ROUGE){ os<< "R";}
            }
            os << std::endl;
    }
    
    return os;
}

Joueur Jeu::getVainqueur() const{
    
     return analyse_grille();
    
}

Joueur Jeu::getJoueurCourant() const {
    return courant;
}

bool Jeu::jouer(int i, int j) {
    if(grille[i][j]== JOUEUR_VIDE){
        modif_grille(i, j);
        modif_courant();
        
        return true;
    }
    else
    {
        return false;    
    }
    
    
}

void Jeu::modif_courant(){
    if (courant == JOUEUR_ROUGE){
        courant = JOUEUR_VERT;
    }
    else{ courant = JOUEUR_ROUGE; }
}

void Jeu::modif_grille(int i, int j){
        grille[i][j] = courant;
}

Joueur Jeu::analyse_grille() const{

    // analyse ligne et colonne
    for(int i = 0; i < 3; i++){
            
        if(grille[i][0] == JOUEUR_ROUGE && grille[i][1] == JOUEUR_ROUGE && grille[i][2] == JOUEUR_ROUGE){ return JOUEUR_ROUGE;}
        else if(grille[i][0] == JOUEUR_VERT && grille[i][1] == JOUEUR_VERT && grille[i][2] == JOUEUR_VERT){ return JOUEUR_VERT;}

        else if(grille[0][i] == JOUEUR_ROUGE && grille[1][i] == JOUEUR_ROUGE && grille[2][i] == JOUEUR_ROUGE){ return JOUEUR_ROUGE;}
        else if(grille[0][i] == JOUEUR_VERT && grille[1][i] == JOUEUR_VERT && grille[2][i] == JOUEUR_VERT){ return JOUEUR_VERT;}
    }
    //analyse diagonale
    if(grille[0][0] == JOUEUR_ROUGE && grille[1][1] == JOUEUR_ROUGE && grille[2][2] == JOUEUR_ROUGE){ return JOUEUR_ROUGE;}
    else if(grille[0][2] == JOUEUR_ROUGE && grille[1][1] == JOUEUR_ROUGE && grille[2][0] == JOUEUR_ROUGE){ return JOUEUR_ROUGE;}

    else if(grille[0][0] == JOUEUR_VERT && grille[1][1] == JOUEUR_VERT && grille[2][2] == JOUEUR_VERT){ return JOUEUR_VERT;}
    else if(grille[0][2] == JOUEUR_VERT && grille[1][1] == JOUEUR_VERT && grille[2][0] == JOUEUR_VERT){ return JOUEUR_VERT;}
    
    //analyse egalite
    else if (check_egalite()){ return JOUEUR_EGALITE;}

    //pas de vainqueur
    else {return JOUEUR_VIDE;}

    
}

bool Jeu::check_egalite() const{
    int compteur = 9;
    for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++)
            { 
                if(grille[i][j] != JOUEUR_VIDE){
                    compteur--;
                }
            }
    }
    if(compteur == 0){
        return true;
    }
    else { return false;}
}
