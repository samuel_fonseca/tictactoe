/// \mainpage
/// 
/// \brief main de tictactoe
/// \n
/// \code{.cpp}
///    Jeu jeu;
///    Joueur vainqueur = jeu.getVainqueur();
///    jeu.raz();
///    int compteur = 0, i, j;
///
///    while(vainqueur == 0){
///    
///        //affichage de jeu
///        cout << jeu << endl;
///
///        cout << "Entrez un coup (i, j) pour " << joueurToNom(jeu.getJoueurCourant()) << endl;
///        cin >> i >> j;
///
///        jeu.jouer(i, j);
///
///        vainqueur = jeu.getVainqueur();
///    }
///    cout << joueurToNom(vainqueur);
///
///    return 0;
/// \endcode
/// \class Jeu
/// \brief test
///