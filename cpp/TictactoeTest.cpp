#include "Tictactoe.hpp"
#include <sstream>

#include <catch2/catch.hpp>

TEST_CASE("test affichage") { 
    Jeu jeu;
    jeu.raz();
    std::ostringstream grille;
    grille << jeu;
    REQUIRE( grille.str() == "...\n...\n...\n");
}

TEST_CASE("test case libres") { 
    Jeu jeu;
    jeu.raz();
    REQUIRE( jeu.jouer(0, 0) == true);

    jeu.modif_grille(0, 1);
    REQUIRE( jeu.jouer(0, 1) == false);
}

TEST_CASE("test detection victiore") { 
    Jeu jeu;
    jeu.raz();

    jeu.modif_grille(0, 1);
    REQUIRE( jeu.analyse_grille() == false);

    jeu.modif_grille(0, 0);
    jeu.modif_grille(0, 1);
    jeu.modif_grille(0, 2);

    REQUIRE( jeu.analyse_grille() == true);
}

TEST_CASE("test detection egalite") { 
    Jeu jeu;
    jeu.raz();
    //8 coups
    jeu.modif_grille(0, 0);
    jeu.modif_grille(0, 1);
    jeu.modif_grille(0, 2);
    jeu.modif_grille(1, 0);
    jeu.modif_grille(1, 1);
    jeu.modif_grille(1, 2);
    jeu.modif_grille(2, 0);
    jeu.modif_grille(2, 1);
    jeu.modif_grille(2, 2);

    REQUIRE( jeu.check_egalite() == true);
}



